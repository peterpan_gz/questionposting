# vim: tabstop=4 shiftwidth=4 softtabstop=4

# Copyright 2012 United States Government as represented by the
# Administrator of the National Aeronautics and Space Administration.
# All Rights Reserved.
#
# Copyright 2012 Nebula, Inc.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from django.conf.urls.defaults import patterns, url
import views
from views import LoginView, auth

urlpatterns = patterns('',
    url(r'^$', views.LoginView, name='login'),
    url(r'^auth/$', views.auth, name='auth'),
    url(r'^lecture/$', views.LectureList.as_view()),
    url(r'^lecture/(?P<pk>[0-9]+)/$', views.LectureDetail.as_view()),
    url(r'^lecture/(?P<lec_pk>[0-9]+)/post/$', views.PostList.as_view()),
    url(r'^lecture/(?P<lec_pk>[0-9]+)/post/(?P<pk>[0-9]+)/$', views.PostDetail.as_view()),    
    )
