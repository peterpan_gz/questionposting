from mobile.models import Lecture, Post
from mobile.serializers import LectureSerializer, PostSerializer
from rest_framework import generics
from django.shortcuts import render
import os
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime,timedelta

def enum(**enums):
    return type('Enum', (), enums)

POSTUPDATEACTION = enum(MOBILESUPPORT=1, PCCLOSED=2, NORMAL=3)

class LectureList(generics.ListCreateAPIView):
    queryset = Lecture.objects.all()
    serializer_class = LectureSerializer


class LectureDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Lecture.objects.all()
    serializer_class = LectureSerializer


class PostList(generics.ListCreateAPIView):
    serializer_class = PostSerializer
    
    """For get posts of a specific lecture """
    def get(self, request, *args, **kwargs):
        lec_pk = kwargs['lec_pk']
        self.queryset = Post.objects.all().filter(lectureKey=lec_pk)
        return super(PostList, self).get(request, *args, **kwargs);
    
    """For create a post of a specific lecture """
    def post(self, request, *args, **kwargs):
        self.lec_pk = kwargs['lec_pk'] # add a temp
        
        self.lectureKey = Lecture.objects.get(pk=self.lec_pk)
        self.posterIp = request.META.get("REMOTE_ADDR", None)
        self.comment   = request.DATA["comment"]
        self.postdate = datetime.now()
        self.support = 0
    
        return super(PostList, self).post(request, *args, **kwargs);
    
    def pre_save(self, obj):
        obj.lectureKey = self.lectureKey
        obj.posterIp = self.posterIp
        obj.comment = self.comment
        obj.postdate = self.postdate
        obj.support = self.support
        
        super(PostList, self).pre_save(obj);
        pass

    def post_save(self, obj, created=False):
        """ update its parent Lecture if required 
        lecture = Lecture.objects.all().filter(pk=self.lec_pk)
        """
        super(PostList, self).post_save(obj, created=False);
        pass

class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    
    def put(self, request, *args, **kwargs):
        self.action = POSTUPDATEACTION.NORMAL
        self.posterIp = ""
        
        if request.method == 'PUT':
            if request.META.get('CONTENT_TYPE', '').startswith('application/json'):
                self.action = POSTUPDATEACTION.MOBILESUPPORT
                self.posterIp = request.META.get("REMOTE_ADDR", None)
            elif request.META.get('CONTENT_TYPE', '').startswith('text/javascript'):
                self.action = POSTUPDATEACTION.PCCLOSED
                
        return super(PostDetail, self).update(request, *args, **kwargs);
    
    def pre_save(self, obj):
        prePost = Post.objects.get(pk=obj.pk)
        obj.posterIp = prePost.posterIp
        obj.comment = prePost.comment
        obj.postdate = prePost.postdate
        obj.support = prePost.support
        obj.answered = prePost.answered

        if self.action == POSTUPDATEACTION.MOBILESUPPORT:
            obj.support = obj.support + 1
            #ip operation
        elif self.action == POSTUPDATEACTION.PCCLOSED:
            obj.answered = True
            
        super(PostDetail, self).pre_save(obj);
        pass
    
def LoginView(request):
    #latest_poll_list = Poll.objects.all().order_by('-pub_date')[:5]
    #context = {'latest_poll_list': latest_poll_list}
    return render(request, './login.html');

@csrf_exempt
def auth(request):
    return HttpResponseRedirect('/mobile/lecture');






    