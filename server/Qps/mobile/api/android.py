from django.http import HttpResponse
from django.shortcuts import render_to_response,get_object_or_404
from django.template import RequestContext
from django.core import serializers

from mobile.models import Lecture,Post
from mobile.common import jasonencoder



def mobile_lecture_add(request, *args, **kwargs):
    print "mobile_lecture_add----------------------------"

    return

def mobile_lecture_update(request, *args, **kwargs):
    print "mobile_lecture_update----------------------------"

    return

def mobile_lecture_get_single(request, *args, **kwargs):
    print "mobile_lecture_get_single----------------------------"
    response=HttpResponse()
    response['Content-Type']="text/javascript"
    lecture = get_object_or_404(Lecture, pk=kwargs['lecture_id'])
    response.write(jasonencoder.jason_encode_qs(lecture))
    return response

def mobile_lecture_get_all(request, *args, **kwargs):
    print "mobile_lecture_get_all----------------------------"
    print "request: {}".format(request.META.get("REMOTE_ADDR", None))
    
    response=HttpResponse()
    response['Content-Type']="text/javascript"
    response.write(jasonencoder.jason_encode_qs(Lecture.objects.all()))
    return response


def mobile_post_add(request, *args, **kwargs):
    print "mobile_post_add----------------------------"

    return

def mobile_post_update(request, *args, **kwargs):
    print "mobile_post_update----------------------------"

    return

def mobile_post_get_single(request, *args, **kwargs):
    print "mobile_post_get_single----------------------------"
    lecture = get_object_or_404(Lecture, pk=kwargs['lecture_id'])
    post = get_object_or_404(Post, lectureKey=lecture.pk, pk=kwargs['post_id'])

    response=HttpResponse()
    response['Content-Type']="text/javascript"
    response.write(jasonencoder.jason_encode_qs(post))
    return response

def mobile_post_get_all(request, *args, **kwargs):
    print "mobile_post_get_all----------------------------"
    print "request: {}".format(request.META.get("REMOTE_ADDR", None))

    lecture = get_object_or_404(Lecture, pk=kwargs['lecture_id'])
    posts = Post.objects.filter(lectureKey__exact=lecture.pk)
    
    
    response=HttpResponse()
    response['Content-Type']="text/javascript"
    response.write(jasonencoder.jason_encode_qs(posts))
    return response

