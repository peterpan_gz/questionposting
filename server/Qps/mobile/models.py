from django.db import models
from datetime import datetime,timedelta


# Create your models here.

class Lecture(models.Model):
    title = models.CharField(max_length=100)
    lecturer = models.CharField(max_length=20)
    lecturerInfo = models.CharField(max_length=400)
    startdate = models.DateTimeField(null = True)
    enddate = models.DateTimeField(null = True)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    location = models.CharField(max_length=30)
    publisher = models.CharField(max_length=20)
    publishDate = models.DateTimeField(null = True)
    closed = models.BooleanField(default = False)
    
    
class Post(models.Model): 
    lectureKey = models.ForeignKey(Lecture)
    posterIp = models.CharField(max_length=40,default = "")
    comment   = models.CharField(max_length=100)
    postdate = models.DateTimeField(default = datetime.now())
    support = models.IntegerField(default = 0)
    answered = models.BooleanField(default = False)

