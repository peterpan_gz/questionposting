from django.forms import widgets
from rest_framework import serializers
from mobile.models import Lecture,Post

class LectureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lecture
        fields = ('id', 'title', 'lecturer', 'lecturerInfo', 'startdate', 'enddate', 'phone', 'email', 'location', 'publisher', 'publishDate', 'closed')

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('id','comment','support','answered','postdate')

    
                