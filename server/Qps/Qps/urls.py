from django.conf.urls import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
from mobile import urls as mobile_urls


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Qps.views.home', name='home'),
    # url(r'^Qps/', include('Qps.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    
    url(r'^mobile/', include(mobile_urls, namespace='mobile')),


    
)
