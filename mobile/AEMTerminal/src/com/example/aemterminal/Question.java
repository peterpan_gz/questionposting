package com.example.aemterminal;

public class Question {

	public Question(int id, String text, String postdate,
			int votes, boolean answered) {
		super();
		this.id = id;
		//this.lectureId = lectureId;
		this.text = text;
		this.postdate = postdate;
		this.votes = votes;
		this.answered = answered;
	}
	
	public int getId() {
		return id;
	}
	public String getText() {
		return text;
	}
	public String getPostdate() {
		return postdate;
	}
	public int getVotes() {
		return votes;
	}
	public boolean getAnswered() {
		return answered;
	}

	//members
	int id;
	String text;
	String postdate;
	int votes;
	boolean answered;
}
