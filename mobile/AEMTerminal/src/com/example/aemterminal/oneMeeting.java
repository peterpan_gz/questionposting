﻿package com.example.aemterminal;

import com.example.aemterminal.MainActivity;
import com.example.aemterminal.ServerInterface;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class oneMeeting<ServiceInterface> extends Activity {

	EditText etMessage; // 内容
	Button bSubmit; // 提交按钮
	int[] msgIds = { R.string.q1, R.string.q2, R.string.q3, R.string.q4,
			R.string.q5 };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questionlist);
		etMessage = (EditText) this.findViewById(R.id.myQestion);// 获取对象
		bSubmit = (Button) this.findViewById(R.id.btnSubmit);// 发送按钮
		ListView questionlv = (ListView) this.findViewById(R.id.Qlistview);

		// 为ListView设置适配器
		BaseAdapter ba = new BaseAdapter() {
			public int getCount() {
				int size = MainActivity.getQuestionList().size();
//				if(size>2){
//					return size-2;
//				}
//				else{
//					return size;
//				}
				return size;
			}

			public Object getItem(int position) {
				return null;
			}

			public long getItemId(int position) {
				return 0;
			}

			public View getView(int arg0, View arg1, ViewGroup arg2) {
				
				List<Question> questionList = MainActivity.getQuestionList();
//				int size = MainActivity.getQuestionList().size();
//				int offset = 0;
//				if(size > 2){
//					offset = 2;
//				}
//				arg0 -= offset;

				//getIntent().getExtras();
				
				/*
				 * 动态生成每个下拉项对应的View，每个下拉项View由LinearLayout
				 * 中包含一个ImageView及一个TextView构成
				 */
				// 初始化LinearLayout
				LinearLayout ll = new LinearLayout(oneMeeting.this);
				ll.setOrientation(LinearLayout.HORIZONTAL); // 设置朝向
				ll.setPadding(5, 5, 5, 5);// 设置四周留白
				
				// 初始化TextView
				TextView tv = new TextView(oneMeeting.this);
				//tv.setText(getResources().getText(msgIds[arg0]));// 设置内容
				String text = questionList.get(arg0).getText() + "  - " + questionList.get(arg0).getVotes();
				tv.setText(text);// 设置内容
				
				tv.setTextSize(24);// 设置字体大小
				tv.setTextColor(oneMeeting.this.getResources().getColor(
						R.color.red));// 设置字体颜色
				tv.setPadding(5, 5, 5, 5);// 设置四周留白
				tv.setGravity(Gravity.LEFT);
				ll.addView(tv);// 添加到LinearLayout中

				// Imeamge button
				ToggleButton tb = new ToggleButton(oneMeeting.this);
				tb.setTextOn("cancel good");
				tb.setTextOff("say good!");
				tb.setChecked(false);
				tb.setTextColor(oneMeeting.this.getResources().getColor(
						R.color.red));
				//tb.setButtonDrawable(0)
				tb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						//setBulbState(isChecked);
						if(isChecked)
						{
						buttonView.setTextColor(oneMeeting.this.getResources().getColor(
						R.color.red));
						}
						else
						{
							buttonView.setTextColor(oneMeeting.this.getResources().getColor(
									R.color.gray));
						}
					}
				});
				ll.addView(tb);
				return ll;
			}
		};

		questionlv.setAdapter(ba);// 为ListView设置内容适配器

		bSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ServerInterface server = MainActivity.getServer();
				
				try {
					
					String text = etMessage.getText().toString();
					//server.postQuestion(2, "i want to go home...");
					server.postQuestion(2, text);
					Log.i("submit", text);
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("Submit Button Clicked");
			}
		});
	}
}