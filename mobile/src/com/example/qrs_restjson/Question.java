package com.example.qrs_restjson;

public class Question {

	public Question(int id, String posterIp, String text, String postdate,
			int votes, boolean answered) {
		super();
		this.id = id;
		//this.lectureId = lectureId;
		this.text = text;
		this.postdate = postdate;
		this.votes = votes;
		this.answered = answered;
	}
	
	//members
	int id;
	String posterIp;
	//int lectureId;
	String text;
	String postdate;
	int votes;
	boolean answered;
}
