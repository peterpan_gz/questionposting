package com.example.qrs_restjson;

public class Conference {

	public Conference(int id, String title, String lecturer,
			String lecturerInfo, String startdate, String enddate,
			String phone, String email, String location, String publisher,
			String publishDate, boolean closed) {
		super();
		this.id = id;
		this.title = title;
		this.lecturer = lecturer;
		this.lecturerInfo = lecturerInfo;
		this.startdate = startdate;
		this.enddate = enddate;
		this.phone = phone;
		this.email = email;
		this.location = location;
		this.publisher = publisher;
		this.publishDate = publishDate;
		this.closed = closed;
	}
	
	public String printSelf(){
		String text = this.toString();
		return text;
	}
	
	//members
//    "id": 1, 
//    "title": "Core & IMS All Employee Meeting September 2013 ", 
//    "lecturer": "Sinisa Krajnovic", 
//    "lecturerInfo": "Vice President, DU Core & IMS, BNET DU CORE & IMS", 
//    "startdate": "2013-10-13T15:30:00Z", 
//    "enddate": "2013-10-13T19:30:00Z", 
//    "phone": "+862089675892", 
//    "email": "sinisa.krajnovic@ericsson.com", 
//    "location": "Guangzhou", 
//    "publisher": "bell zhong", 
//    "publishDate": "2013-10-03T20:00:00Z", 
//    "closed": false
    
    private int id;
    private String title;
    private String lecturer;
    private String lecturerInfo;
    private String startdate;
    private String enddate;
    private String phone;
    private String email;
    private String location;
    private String publisher;
    private String publishDate;
    private boolean closed;

}
