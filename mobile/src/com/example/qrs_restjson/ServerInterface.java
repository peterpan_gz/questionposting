package com.example.qrs_restjson;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ServerInterface {
	
	public final int MaxConferenceNumber_c = 20;
	public final int MaxQuestionNumber_c = 200;
	
	public ServerInterface(String serverAddress) {
		super();
		this.serverAddress = serverAddress;
	}

	//public List<Conference> getConferenceList() throws Exception {
	public String getConferenceList() throws Exception {
		//String confUrl = serverAddress+"/mobile";
		String confUrl = serverAddress+"/lecture";

		String result = httpGet(confUrl);

		Log.i("getConferenceList", result);

		List<Conference> confList = new ArrayList<Conference>(MaxConferenceNumber_c);

		try {
			//JSONArray items = jsonResult.getJSONArray("items");
			JSONArray items = new JSONArray(result);

			Log.i("getConferenceList", items.length()+"");

			for (int i = 0; i < items.length(); i++) {
				JSONObject obj = items.getJSONObject(i);
				Log.i("getConferenceList", obj.toString(1));

				if(confList.size() < MaxConferenceNumber_c){
					confList.add(
							new Conference(obj.getInt("id"), 
									obj.getString("title"), 
									obj.getString("lecturer"), 
									obj.getString("lecturerInfo"), 
									obj.getString("startdate"), 
									obj.getString("enddate"), 
									obj.getString("phone"), 
									obj.getString("email"), 
									obj.getString("location"), 
									obj.getString("publisher"), 
									obj.getString("publishDate"), 
									obj.getBoolean("closed")));
				}
				else{
					Log.w("getConferenceList", "Exceeding max number of conferences supported: "+MaxConferenceNumber_c);
					break;
				}
			}
		} catch (JSONException e) {
			// handle exception
		}

		//result = "confList size = " + confList.size() + "\n";
		for(int i=0; i<confList.size(); i++){
			result += confList.get(i).toString();
		}
		result += "\n>>>>" + "confList size = " + confList.size();
		Log.i("getConferenceList", result);

		return result;
		//return confList;
	}

	//public List<Question> getQuestionList(String id) throws Exception {
	public String getQuestionList(int id) throws Exception {
		String questionUrl = serverAddress+"/lecture/"+id+"/post";

		String result = httpGet(questionUrl);

		Log.i("getQuestionList", result);

		List<Question> questionList = new ArrayList<Question>(MaxQuestionNumber_c);

		try {
			JSONArray items = new JSONArray(result);

			Log.i("getQuestionList", items.length()+"");

			for (int i = 0; i < items.length(); i++) {
				JSONObject obj = items.getJSONObject(i);
				Log.i("getQuestionList", obj.toString(1));

				if(questionList.size() < MaxQuestionNumber_c){
					questionList.add(
							new Question(obj.getInt("id"),
									//obj.getInt("lectureId"), 
									obj.getString("posterIp"),
									obj.getString("comment"), 
									obj.getString("postdate"), 
									obj.getInt("support"),
									obj.getBoolean("answered")));
				}
				else{
					Log.w("getQuestionList", "Exceeding max number of questions supported: "+MaxQuestionNumber_c);
					break;
				}
			}
		} catch (JSONException e) {
			// handle exception
		}

		//result = "confList size = " + confList.size() + "\n";
		Log.i("getQuestionList", questionList.size()+"");
		for(int i=0; i<questionList.size(); i++){
			result += questionList.get(i).toString();
		}
		result += "\n>>>>" + "questionList size = " + questionList.size();
		Log.i("getQuestionList", result);

		return result;
		//return questionList;
	}

	public String postQuestion(int lectureId, String name, String text) throws Exception {
		String questionUrl = serverAddress+"/lecture/"+lectureId+"/post";

		String content = "{\"name\":\""+name+"\",\"text\":\""+text+"\"}";
		Log.i("postQuestion", content);
		
		String result = httpPost(questionUrl, content);

		return result;
	}
	
	public String voteQuestion(int lectureId, int id, boolean support) throws Exception {
		String questionUrl = serverAddress+"/lecture/"+lectureId+"/post/"+id+"/";

		String content;
		if(support){
			content = "{support:1}";
		}
		else{
			content = "{support:0}";
		}
		
		String result = httpPost(questionUrl, content);

		return result;
	}

	private String httpGet(String path) throws Exception {
		URL url = new URL(path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setConnectTimeout(5 * 1000);
		InputStream inStream = conn.getInputStream();
		byte[] data = readFromInput(inStream);
		String html = new String(data, "UTF-8");
		
		conn.disconnect();
		return html;
	}

	private String httpPost(String path, String postContent) throws Exception {
		URL url = new URL(path);
		String result = "";

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		try {
			conn.setDoOutput(true);
			conn.setChunkedStreamingMode(0);

			OutputStream out = new BufferedOutputStream(conn.getOutputStream());
			out.write(postContent.getBytes());
			//writeStream(out);

			//DataOutputStream dos = new DataOutputStream(httpConn.getOutputStream());
			//
			//String postContent = URLEncoder.encode("channel", "UTF-8") + "=" + URLEncoder.encode("Devdiv", "UTF-8") + "&" + URLEncoder.encode("author", "UTF-8") + "="+ URLEncoder.encode("Sodino", "UTF-8") ;
			//dos.write(postContent.getBytes());
			//dos.flush();
			// 执行完dos.close()后，POST请求结束
			//dos.close();
			
			InputStream in = new BufferedInputStream(conn.getInputStream());
			byte[] data = readFromInput(in);
			result = new String(data, "UTF-8");
			Log.i("httpPost", "return code: "+result);
		}
		finally {
			conn.disconnect();
		}

		return result;
	}


	private static byte[] readFromInput(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		inStream.close();
		return outStream.toByteArray();
	}

	private String serverAddress;
}
