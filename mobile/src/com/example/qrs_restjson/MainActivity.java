package com.example.qrs_restjson;


//import java.io.ByteArrayOutputStream;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.URL;
import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity {
	//public final String ServerIp_c = "http://146.11.7.32";
	public final String ServerIp_c = "http://146.11.1.67:8000/mobile";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.main);
 
       // to avoid mainthread network exception
       StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
	       .detectDiskReads()
	       .detectDiskWrites()
	       .detectNetwork()   // or .detectAll() for all detectable problems       
	       .penaltyLog()
	       .build());
	   StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()       
		   .detectLeakedSqlLiteObjects()
		   .penaltyLog()
		   .penaltyDeath()
		   .build());

       TextView textView = (TextView) this.findViewById(R.id.textView);
       try {
    	   ServerInterface server = new ServerInterface(ServerIp_c);
    	   
    	   //String result = server.getConferenceList();
    	   String result = server.getQuestionList(1);
    	   //String result = server.voteQuestion(1, 1, true);
    	   //String result = server.postQuestion(1, "", "i want a girl");
    	   
    	   textView.setText(result);
    	   
       } catch (Exception e) {
           Log.e("MainActivity", e.toString());
           Toast.makeText(MainActivity.this, "Network connection failure", 1).show();
       }

    }

}
